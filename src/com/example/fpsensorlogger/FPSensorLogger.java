package com.example.fpsensorlogger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Date;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



public class FPSensorLogger extends Activity implements SensorEventListener  {
   // for the accel data
	private SensorManager mSensorManager;
	private Sensor mSensorLA;
    TextView 	mTvAccelStatus;
    TextView 	mTvNoiseStatus;
    TextView 	mTvAppStatus;
    Button btnStartlog;
    Button btnStoplog;
    
    // for noise recog.
    
    private MediaRecorder mediaRecorder;
    private boolean isActive = false;
    private Handler pollHandler = new Handler();    
    private File tempAudioFile;
    
    // for logging
    boolean mLog = false;
    String accelFN = "accelFinger.log";
	String micFN = "micFinger.log";
	 
    private File				 mLogfileA, mLogfileS;
	public FileOutputStream  			 mLogStreamA, mLogStreamS;
	public OutputStreamWriter           mLogWriterA, mLogWriterS;
	
    
    final String TAG = "FPSensorLogger";
	private void initializeSensors() {
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		//mSensorLA = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
		mSensorLA = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mSensorManager.registerListener(this, mSensorLA, mSensorManager.SENSOR_DELAY_UI);
	}
	
	private void initializeAudioRecording() {
		mediaRecorder = new MediaRecorder();
		if(prepareMediaRecorder()) {
			mediaRecorder.start();
			noisePoller.run();
		} else {
			mTvNoiseStatus.setText("record failed");
		}
	}
	
	 public boolean createLogFiles() {
		 
	    	Log.e(TAG, "log file dir:"); 
	        Log.e(TAG, Environment.getExternalStorageDirectory().toString());
	    	File fileA = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), accelFN );
	    	File fileS = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), micFN  );

	        try {
	        	if(fileA.exists() == false) {
	        		fileA.createNewFile();
	        	}
	        	if(fileA.canWrite() == false) {
	        		Log.e(TAG, "Cannot write to file: " + fileA.toString());        		
	        	} else {
	        		Log.e(TAG, "Can write to file: " + fileA.toString());
	        	}
	        	if(fileS.exists() == false) {
	        		fileS.createNewFile();
	        	}
	        	if(fileS.canWrite() == false) {
	        		Log.e(TAG, "Cannot write to file: " + fileS.toString());        		
	        	} else {
	        		Log.e(TAG, "Can write to file: " + fileS.toString());
	        	}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				Log.e(TAG, "Could not create log file in dir:" + Environment.getExternalStorageDirectory().toString());
	      	  	return false;
			}
	        try {
	        	  mLogStreamA = new FileOutputStream(fileA);
	        	  mLogStreamS = new FileOutputStream(fileS);
	    	} catch (Exception e) {
	        	  e.printStackTrace();
	        	  Log.e(TAG, "Could not create mLogStream");
	        	  return false;
	    	}
	        try {
	      	     
	      	  mLogWriterA = new  OutputStreamWriter(mLogStreamA);
	      	  mLogWriterS = new  OutputStreamWriter(mLogStreamS);
		  	} catch (Exception e) {
		      	  e.printStackTrace();
		      	  Log.e(TAG, "Could not create mLogWriter");
		      	  return false;
		  	}
	        return true;  
	    }
	    
	
	private boolean prepareMediaRecorder()
    {

        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        File dir = getCacheDir();
        try {
            tempAudioFile = File.createTempFile("tempmic", ".3gp", dir);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        mediaRecorder.setOutputFile(tempAudioFile.getAbsolutePath());
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
	
	private Runnable noisePoller = new Runnable() {
        @Override
        public void run() {

        	float maxAmp = (float) mediaRecorder.getMaxAmplitude() / 32767.0f;
            // http://stackoverflow.com/questions/10668470/what-is-the-unit-of-the-returned-amplitude-of-getmaxamplitude-method
            // --> this a value  [0 ... 32.767] corresponding to the max volatage
            // it is not easily converted to physical units as it depends on how the mic converts pressure to voltage
            // --> show a normalized value between 0 ... 1
            //double maxDecibel =  20 * Math.log10(maxAmp / 2700.0);

            // log
           // mTvNoiseStatus.setText("" + maxAmp/(double)Integer.MAX_VALUE * 10000);
          // mTvNoiseStatus.setText("noise: " + (float) (maxAmp/(double)32767));
        	 mTvNoiseStatus.setText("noise: " + maxAmp);
            
            if(mLog) {
            	logMicData(maxAmp);
            }
            //set poll timer
            pollHandler.postDelayed(noisePoller, 50);
        }
    };

  private void logAccelData(float z) {
    	// x,y,z,pitch(x), roll(y)
    	String str = new String(System.currentTimeMillis() + ";" + z + "\n");
    	Log.d("FTDataFP", str);
    	try {      	         
    		mLogWriterA.append(str);
	  	} catch (Exception e) {
	      	  e.printStackTrace();
  	  }
    }
  
  private void logMicData(float val) {
  	// x,y,z,pitch(x), roll(y)
  	String str = new String(System.currentTimeMillis() + ";" + val + "\n");
  	
  	try {      	         
  		mLogWriterS.append(str);
	  	} catch (Exception e) {
	      	  e.printStackTrace();
	  }
  }
  
  
  	public void startLog(View v) {
	    mLog = true;
        mTvAppStatus.setText("logging");
	}
  	
  	public void stopLog(View v) {
	    mLog = false;
        mTvAppStatus.setText("not logging");
	} 
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fpsensor_logger); 
		
		mTvAccelStatus = (TextView) findViewById(R.id.tvAccel);
		mTvNoiseStatus = (TextView) findViewById(R.id.tvNoise);
		mTvAppStatus  = (TextView) findViewById(R.id.tvStatus);
		btnStartlog = (Button) findViewById(R.id.startButton);
		btnStoplog = (Button) findViewById(R.id.stopButton);

		
//		btnStartlog.setOnClickListener(new Button.OnClickListener() {
//	         public void onClick(View v) {
//	            mLog = true;
//	            mTvAppStatus.setText("logging");
//	         } });
//		
//		btnStartlog.setOnClickListener(new Button.OnClickListener() {
//	         public void onClick(View v) {
//	            mLog = false;
//	            mTvAppStatus.setText("not logging");
//	         } });
		
		
		
		initializeSensors();
		initializeAudioRecording();
		createLogFiles();
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.fpsensor_logger, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		 
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		//if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

			mTvAccelStatus.setText("accel z: " + event.values[2]);
			if(mLog) {
            	logAccelData(event.values[2]);
            }

	    }
		
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}
}
